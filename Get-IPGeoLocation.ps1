<#
.Synopsis
    Short description
.DESCRIPTION
    Long description
.EXAMPLE
    Example of how to use this cmdlet
.EXAMPLE
    Another example of how to use this cmdlet
#>
function Get-IPGeoLocation
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
                    ValueFromPipelineByPropertyName=$true,
                    Position=0)]
        [IPAddress[]]$IPAddress
    )
        
    Begin
    {
    }
    Process
    {
        foreach ($Item in $IPAddress)
        {
            Write-Verbose "Querying $Item"
            $resource = "http://freegeoip.net/xml/$Item"

            $geoip = Invoke-RestMethod -Method Get -URI $resource

            $hash = @{
                IP = $geoip.Response.IP
                CountryCode = $geoip.Response.CountryCode
                CountryName = $geoip.Response.CountryName
                RegionCode = $geoip.Response.RegionCode
                RegionName = $geoip.Response.RegionName
                City = $geoip.Response.City
                ZipCode = $geoip.Response.ZipCode
                TimeZone = $geoip.Response.TimeZone
                Latitude = $geoip.Response.Latitude
                Longitude = $geoip.Response.Longitude
                MetroCode = $geoip.Response.MetroCode
                }

            $result = New-Object PSObject -Property $hash

            return $result
        }
    }
    End
    {
    }
}